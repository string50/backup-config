(setq my-name "str50")
(setq my-address "str50@718.be")


(require 'package)
(defvar my/vendor-dir (expand-file-name "vendor" user-emacs-directory))
(add-to-list 'load-path my/vendor-dir)

(dolist (project (directory-files my/vendor-dir t "\\w+"))
  (when (file-directory-p project)
    (add-to-list 'load-path project)))

(add-to-list 'package-archives
             '("melpa" . "http://melpa.org/packages/"))
(package-initialize)
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package)
  (message "Installed use-package."))

(eval-when-compile
  (require 'cl)
  (require 'use-package)
  ;;; To force all package to check, if not present install
  (setq use-package-always-ensure t)
  (setq use-package-verbose t))


;;; Read other files
(defconst user-init-dir
  ;;; Set the init files dir
  ;;; user-emacs-directory -> user_init_directory ->  ~/.emacs.d/
  (cond ((boundp 'user-emacs-directory)
         user-emacs-directory)
        ((boundp 'user-init-directory)
         user-init-directory)
        (t "~/.emacs.d/")))

(defun load-user-file (file)
  (interactive "f")
  "Load a file in current user's configuration directory"
  (load-file (expand-file-name file user-init-dir)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Org mode
(defun org-habit-config ()
  (setq org-habit-preceding-days 7
        org-habit-following-days 1
        org-habit-graph-column 80
        org-habit-show-habits-only-for-today t
        org-habit-show-all-today t))

(defun org-babel-config ()
  (org-babel-do-load-languages
   'org-babel-load-languages
   '((sh . t)
     (C . t)))
  (setq org-src-fontify-natively t
        org-confirm-babel-evaluate nil)

  (add-hook 'org-babel-after-execute-hook (lambda ()
                                            (condition-case nil
                                                (org-display-inline-images)
                                              (error nil)))
            'append))


(use-package org
  :bind (("C-c a" . org-agenda)
         ("C-c c" . org-capture)
	 ("C-c l" . org-store-link))
  :config
  (add-to-list 'org-modules 'org-habit)
  (add-hook 'org-mode-hook (lambda ()
                             (flyspell-mode 1)
                             (writegood-mode)))
  (org-load-modules-maybe t)
  (setq org-log-done t
        org-todo-keywords '((sequence "TODO" "INPROGRESS" "DONE"))
        org-todo-keyword-faces '(("INPROGRESS" . (:foreground "blue" :weight bold))))
  (setq org-agenda-show-log t
        org-agenda-todo-ignore-scheduled t
        org-agenda-todo-ignore-deadlines t)
  (setq org-agenda-files (list "~/Dropbox/org/personal.org"
                               "~/Dropbox/org/groupon.org"))
  ;;(org-habit-config)
  (org-babel-config)
  )
;;; end of org mode
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; (use-package gist)

(use-package magit
  :bind (("C-c g" . magit-status))
  :commands magit-status)

(use-package smartparens-config
  :ensure smartparens
  :bind (
	 ("C-c (" . wrap-with-parens)
	 ("C-c ["  . wrap-with-brackets)
	 ("C-c {"  . wrap-with-braces)
	 ("C-c '"  . wrap-with-single-quotes)
	 ("C-c \"" . wrap-with-double-quotes)
	 ("C-c _"  . wrap-with-underscores)
	 ("M-[" . sp-backward-unwrap-sexp)
	 ("M-]" . sp-unwrap-sexp)
	 ("C-<right>" . sp-forward-slurp-sexp)
	 ("M-<right>" . sp-forward-barf-sexp)
	 ("C-<left>"  . sp-backward-slurp-sexp)
	 ("M-<left>"  . sp-backward-barf-sexp))
  :config
  (progn
    (show-smartparens-global-mode t)
    (add-hook 'prog-mode-hook 'turn-on-smartparens-strict-mode)
    (add-hook 'markdown-mode-hook 'turn-on-smartparens-strict-mode)))

;;; flycheck for syntax checker; still need lang-specific syntax checker to work
(use-package flycheck
  :commands global-flycheck-mode)

;;; writegood-mode
(use-package writegood-mode
  :commands writegood-mode)


(use-package nodejs-repl
  :commands nodejs-repl)

(use-package htmlize)

(use-package helm)
(use-package helm-gtags
  :commands helm-gtags-mode
  :init
  (add-hook 'c-mode-hook 'helm-gtags-mode)
  (add-hook 'c++-mode-hook 'helm-gtags-mode)  
  :config
  )

(use-package company
  :commands company-mode
  :init
  (add-hook 'prog-mode-hook 'company-mode)
  :config
  (use-package company-c-headers
    :config
    (add-to-list 'company-backends 'company-c-headers)
    (add-to-list 'company-c-headers-path-system "/usr/include/c++/5.1.1")))

(use-package smex
  :bind (("M-x" . smex)
         ("M-X" . smex-major-mode-commands)))

;;; Ido
(use-package ido
  :config
  (ido-mode t)
  (setq ido-enable-flex-matching t
        ido-use-virtual-buffers t))


;;; Language and Hooks

;; generic c family hook

;; c/c++ lang
(load-user-file "google-c-style.el")
(add-hook 'c-mode-common-hook (lambda ()
                                (google-set-c-style)
				))

;; Lisp-y langs
(defvar lisp-modes)
(setq lisp-modes '(lisp-mode
                   emacs-lisp-mode
                   common-lisp-mode
                   scheme-mode
                   clojure-mode))
(defun generic-lisp-hook ()
  )
(dolist (mode lisp-modes)
  (add-hook (intern (format "%s-hook" mode))
            'generic-lisp-hook))


;;; c#-mode
(use-package csharp-mode
  :commands csharp-mode)

;;; go
(use-package go-mode
  :commands go-mode)

;;; haskell
(use-package haskell-mode
  :mode ("\\.hs\\'")
  :config
  (add-hook 'haskell-mode-hook 'haskell-indentation-mode))

;;; markdown
(defun markdown-custom ()
  "Markdown custom hook"
  (visual-line-mode t)
  (writegood-mode t)
  (flyspell-mode 1)
  (setq markdown-command "pandoc --smart -f markdown -t html")
  (setq markdown-css-paths (expand-file-name "markdown.css" my/vendor-dir)))
(use-package markdown-mode
  :config
  (add-hook 'markdown-mode-hook 'markdown-custom))

;;; Python
(use-package python-mode
  :mode ("\\.py\\'")
  :config
  (setq python-indent-offset 4))
(use-package company-jedi
  :config
  (add-to-list 'company-backends 'company-jedi))

;;; web
(use-package web-mode
  :mode ("\\.phtml\\'"
	 "\\.tpl\\.php\\'"
	 "\\.[agj]sp\\'"
	 "\\.html?\\'"))

;;; sml-mode
(use-package sml-mode)

;;; yaml-mode
(use-package yaml-mode
  :mode ("\\.yml\\'" "\\.yaml\\'"))

;;; asciidoc mode
(use-package adoc-mode
  :mode ("\\.adoc\\'" "\\.asciidoc\\'"))

;;; sass mode
(use-package sass-mode
  :mode ("\\.scss\\'"))

;; shell script
(add-to-list 'auto-mode-alist '("\\.zsh$" . shell-script-mode))

;; coffee script

;;; End of langs and hooks

;;(setq backup-directory-alist `((".*" . ,temporary-file-directory)))
;;(setq auto-save-file-name-transforms `((".*" ,temporary-file-directory t)))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; User Appliation Level packages
;;; Deft
(use-package deft
  :bind (("<f8>" . deft))
  :config
  (setq deft-directory "~/notes"
        deft-use-filename-as-title t
        deft-extensions '("org" "md" "txt" "tex")))

;;; o-blog
(use-package o-blog)

;;; Spell Check
(use-package flyspell
  :config
  (setq flyspell-issue-welcome-flag nil)
  (if (eq system-type 'darwin)
      (setq-default ispell-program-name "/usr/local/bin/aspell")
    (setq-default ispell-program-name "/usr/bin/aspell")))

;;; Smart compile
(use-package smart-compile
  :bind (("<f5>" . smart-compile))
  :config (setq smart-compile-alist
                '((emacs-lisp-mode emacs-lisp-byte-compile)
                  (html-mode browse-url-of-buffer)
                  (nxhtml-mode browse-url-of-buffer)
                  (html-helper-mode browse-url-of-buffer)
                  (octave-mode run-octave)
                  ("\\.c\\'" . "gcc -O2 %f -lm -o %n")
                  ("\\.[Cc]+[Pp]*\\'" . "g++ %f -std=c++11 -g -Wall -o %n")
                  ("\\.m\\'" . "gcc -O2 %f -lobjc -lpthread -o %n")
                  ("\\.java\\'" . "javac %f")
                  ("\\.php\\'" . "php -l %f")
                  ("\\.f90\\'" . "gfortran %f -o %n")
                  ("\\.[Ff]\\'" . "gfortran %f -o %n")
                  ("\\.cron\\(tab\\)?\\'" . "crontab %f")
                  ("\\.tex\\'" . "xelatex %f")
                  ("\\.texi\\'" . "makeinfo %f")
                  ("\\.mp\\'" . "mptopdf %f")
                  ("\\.pl\\'" . "perl %f")
                  ("\\.rb\\'" . "ruby %f")
		  ("\\.adoc\\'" . "asciidoc %f"))))

(use-package dired+
  :config)

;;; minor mode for Emacs that displays the key bindings following your currently entered incomplete command (a prefix) in a popup. Default is to display hint after 1 second
(use-package which-key)

;;; End of User Application Level packages
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;; Editor Settings

;;; Convenience Function
(defun untabify-buffer ()
  (interactive)
  (untabify (point-min) (point-max)))

(defun indent-buffer ()
  (interactive)
  (indent-region (point-min) (point-max)))

(defun cleanup-buffer ()
  "Perform a bunch of operations on the whitespace content of a buffer."
  (interactive)
  (indent-buffer)
  (untabify-buffer)
  (delete-trailing-whitespace))

(defun cleanup-region (beg end)
  "Remove tmux artifacts from region."
  (interactive "r")
  (dolist (re '("\\\\│\·*\n" "\W*│\·*"))
    (replace-regexp re "" nil beg end)))

;;; End of Convenience Function

;;; Keyboard
(setq echo-keystrokes 0.1
      ;;use-dialog-box nil
      visible-bell nil)
(defalias 'yes-or-no-p 'y-or-n-p)

(global-set-key (kbd "RET") 'newline-and-indent)
(global-set-key (kbd "C-;") 'comment-or-uncomment-region)
(global-set-key (kbd "M-/") 'hippie-expand)
(global-set-key (kbd "C-+") 'text-scale-increase)
(global-set-key (kbd "C--") 'text-scale-decrease)
(global-set-key (kbd "C-x C-b") 'ibuffer)
(global-set-key (kbd "s-<tab>") 'other-window)
(global-set-key (kbd "C-x M-t") 'cleanup-region)
(global-set-key (kbd "C-c n") 'cleanup-buffer)
(global-set-key (kbd "C-x k") 'kill-this-buffer)



;;; End of keyboard

;;; Preferences
(setq-default show-trailing-whitespace nil)
(setq column-number-mode t)

(setq inhibit-splash-screen t
      initial-scratch-message nil
      initial-major-mode 'org-mode)

(scroll-bar-mode 1)
(tool-bar-mode -1)
(menu-bar-mode 1)



(delete-selection-mode t)
(transient-mark-mode t)
(setq x-select-enable-clipboard t)

(setq-default indicate-empty-lines nil)

(setq tab-width 4
      indent-tabs-mode nil)
(setq make-backup-files nil)

;;; Linum
;; (use-package linum
;;   :config
;;   (global-linum-mode t)
;;   (setq linum-format "%4d\u2502")
;;   )
;; (set-face-attribute 'linum nil
;; 		    :foreground (face-foreground 'default)
;; 		    :background (face-background 'default))


;;; Parentheses
(show-paren-mode t)


(setq-default cursor-type '(bar . 2))
(blink-cursor-mode t)
;(set-cursor-color "#000000")
(setq gdb-many-windows t)

(use-package powerline
  :config
  (powerline-default-theme))

;; (use-package base16-theme)
;; (load-theme 'base16-atelierforest-light t)
;; (set-face-attribute 'fringe nil
;;                       :foreground (face-foreground 'default)
;;                       :background (face-background 'default))

(use-package tangotango-theme)

(use-package ansi-color)
(defun colorize-compilation-buffer ()
  (read-only-mode 0)
  (ansi-color-apply-on-region (point-min) (point-max))
  (read-only-mode 1))
(add-hook 'compilation-filter-hook 'colorize-compilation-buffer)

;;; End of Preferences
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:family "Fira Code" :slant normal :weight normal :height 105 :width normal :foundry "FBI ")))))
